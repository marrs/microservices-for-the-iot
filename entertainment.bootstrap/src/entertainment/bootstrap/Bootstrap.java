package entertainment.bootstrap;

import java.net.ServerSocket;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Bootstrap implements BundleActivator {
	public void start(final BundleContext context) throws Exception {
		if (System.getProperty("org.osgi.service.http.port") == null) {
			ServerSocket s = new ServerSocket(0);
			int port = s.getLocalPort();
			System.out.println("bound to port: " + port);
			System.setProperty("org.osgi.service.http.port", "" + port);
			s.close();
		}
	}

	public void stop(BundleContext context) throws Exception {}
}
