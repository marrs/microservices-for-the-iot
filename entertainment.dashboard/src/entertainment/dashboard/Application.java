package entertainment.dashboard;

import java.util.Map;

/**
 * Application Service.
 */
public interface Application {
	/**
	 * Returns the name of this application.
	 */
	String getName();
	
	/**
	 * Returns status information about the application.
	 */
	Map<String, String> getStatus();
	
	/**
	 * Executes a command to interact with the application.
	 */
	String execute(String cmd) throws Exception;
}
