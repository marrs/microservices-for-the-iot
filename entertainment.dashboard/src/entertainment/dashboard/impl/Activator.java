package entertainment.dashboard.impl;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

import entertainment.dashboard.Application;

public class Activator extends DependencyActivatorBase {
	@SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Object.class.getName(), new Properties() {{
				put(CommandProcessor.COMMAND_SCOPE, "dash");
				put(CommandProcessor.COMMAND_FUNCTION, new String[] { "listApps", "appStatus", "appExec" });
				}})
			.setImplementation(Dashboard.class)
			.add(createServiceDependency().setService(Application.class).setRequired(false).setCallbacks("add", "remove"))
		);
	}
}
