package entertainment.dashboard.impl;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import entertainment.dashboard.Application;

@Path("/dashboard")
public class Dashboard {
	private CopyOnWriteArrayList<Application> m_apps = new CopyOnWriteArrayList<>();
	
	void add(Application a) {
		m_apps.add(a);
	}
	
	void remove(Application a) {
		m_apps.remove(a);
	}
	
	@GET @Path("apps")
	public List<Application> getApps() {
		return m_apps;
	}

	public void listApps() {
		List<Application> apps = getApps();
		if (apps == null || apps.size() == 0) {
			System.out.println("No applications installed.");
			return;
		}
		for (int i = 0; i < apps.size(); i++) {
			Application a = apps.get(i);
			try {
				System.out.println("[" + (i + 1) + "] " + a.getName());
			}
			catch (RuntimeException e) {
				System.out.println("[" + (i + 1) + "] (unreachable)");
			}
		}
	}
	
	public void appStatus(int index) {
		List<Application> apps = m_apps;
		Application a = apps.get(index - 1);
		try {
			System.out.println("status: " + a.getStatus());
		}
		catch (RuntimeException e) {
			System.out.println("status: (unreachable) (cause: " + e.getMessage() + ")");
		}
	}
	
	public String appExec(int index, String cmd) {
		List<Application> apps = m_apps;
		Application a = apps.get(index - 1);
		try {
			return a.execute(cmd);
		}
		catch (Exception e) {
			return "exec: (unreachable) (cause: " + e.getMessage() + ")";
		}
	}
}
