package entertainment.phone;

import java.util.HashMap;
import java.util.Map;

import entertainment.dashboard.Application;

public class PhoneApplication implements Application {
	private Map<String, String> m_status = new HashMap<>();

	@Override
	public String getName() {
		return "Phone";
	}

	@Override
	public Map<String, String> getStatus() {
		m_status.put("carrier", "T-Mobile");
		m_status.put("speed", "4G");
		m_status.put("phone", "idle");
		return m_status;
	}

	@Override
	public String execute(String cmd) {
		return "";
	}
}
