package entertainment.phone;



import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

import entertainment.dashboard.Application;

public class Activator extends DependencyActivatorBase {
	@SuppressWarnings("serial")
	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Application.class.getName(), new Properties() {{ put(RemoteConstants.SERVICE_EXPORTED_INTERFACES, Application.class.getName()); }})
			.setImplementation(PhoneApplication.class)
		);
	}
	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {
	}
}
