package entertainment.weatherreport;

import java.util.HashMap;
import java.util.Map;

import entertainment.dashboard.Application;

public class WeatherReportApplication implements Application {
	private Map<String, String> m_status = new HashMap<>();

	@Override
	public String getName() {
		return "Weather Report";
	}

	@Override
	public Map<String, String> getStatus() {
		m_status.put("temperature", "21 degrees");
		m_status.put("city", "San Francisco");
		m_status.put("skies", "clear");
		return m_status;
	}

	@Override
	public String execute(String cmd) {
		return "";
	}
}
