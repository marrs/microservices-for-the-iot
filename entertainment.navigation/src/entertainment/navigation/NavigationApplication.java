package entertainment.navigation;

import java.util.HashMap;
import java.util.Map;

import entertainment.dashboard.Application;

public class NavigationApplication implements Application {
	private Map<String, String> m_status = new HashMap<>();

	@Override
	public String getName() {
		return "Navigation";
	}

	@Override
	public Map<String, String> getStatus() {
		m_status.put("position", "37.7854115,-122.4108005");
		m_status.put("goal", "37.8081187,-122.4210084");
		m_status.put("gps", "offline");
		return m_status;
	}

	@Override
	public String execute(String cmd) {
		return "";
	}
}
