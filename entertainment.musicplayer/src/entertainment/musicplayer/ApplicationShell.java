package entertainment.musicplayer;

import java.util.Map;

import entertainment.dashboard.Application;

public class ApplicationShell {
	private volatile Application m_app;
	
	public String exec(String cmd) throws Exception {
		return m_app.execute(cmd);
	}
	
	public Map<String, String> status() {
		return m_app.getStatus();
	}
}
