package entertainment.musicplayer;

import java.io.IOException;

public class Player implements Runnable {
	private volatile PlayList m_playList;
	private volatile Song m_currentSong = Song.EMPTYSONG;
	private volatile Thread m_playerThread;
	private volatile boolean m_isRunning;
	private volatile boolean m_isPlaying;
	
	public void start() {
		m_playerThread = new Thread(this, "Player");
		m_isRunning = true;
		m_playerThread.start();
	}
	
	public void run() {
		while (m_isRunning) {
			try {
				Thread.sleep(1000);
				if (m_isPlaying) {
					if (m_currentSong.isDone()) {
						m_currentSong = m_playList.getNextSong();
						m_currentSong.play();
					}
				}
			}
			catch (IOException e) {
				System.out.println("Could not play " + m_currentSong + " because: " + e);
			}
			catch (InterruptedException e) {
			}
		}
	}
	
	public void stop() {
		m_isRunning = false;
		m_playerThread.interrupt();
	}
	
	public synchronized void playSong() throws IOException {
		if (!m_isPlaying) {
			m_isPlaying = true;
			if (!m_currentSong.isPlaying()) {
				if (m_currentSong.isDone()) {
					m_currentSong = m_playList.getNextSong();
				}
				m_currentSong.play();
			}
		}
	}
	
	public synchronized void stopSong() {
		if (m_isPlaying) {
			m_isPlaying = false;
			if (m_currentSong.isPlaying()) {
				m_currentSong.stop();
			}
		}
	}
	
	public boolean isPlaying() {
		return m_currentSong.isPlaying();
	}

	public String getSongName() {
		return m_currentSong.getName();
	}
}
