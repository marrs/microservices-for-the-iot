package entertainment.musicplayer;

import java.io.File;
import java.io.IOException;

public class Song {
	private String m_name;
	private File m_file;
	private boolean m_isPlaying;
	private Process m_playerProcess;

	public boolean isPlaying() {
		return m_isPlaying;
	}

	public synchronized void stop() {
		if (m_playerProcess != null) {
			m_playerProcess.destroy();
			m_playerProcess = null;
			m_isPlaying = false;
		}
	}

	public boolean isDone() {
		return (m_playerProcess != null && !m_playerProcess.isAlive() && m_isPlaying);
	}

	public synchronized void play() throws IOException {
		if (!m_isPlaying) {
			m_isPlaying = true;
			m_playerProcess = Runtime.getRuntime().exec("afplay " + m_file);
		}
	}

	public String getName() {
		return m_name;
	}
	
	public static Song create(File file) {
		Song song = new Song();
		song.m_file = file;
		song.m_name = file.getName();
		return song;
	}

	public static final Song EMPTYSONG = new Song() {
		public boolean isDone() { return true; };
		public boolean isPlaying() {return false; };
		public void play() {};
		public void stop() {};
		public String getName() { return "<none>"; };
	};
}
