package entertainment.musicplayer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class PlayList {
	private Queue<Song> m_songs = new LinkedBlockingQueue<>();

	void start() {
		addFolder(new File("/Users/marrs/Music/"));
	}
	
	public synchronized Song getNextSong() {
		Song song = m_songs.poll();
		return song == null ? Song.EMPTYSONG : song;
	}
	
	public synchronized Song[] getPlayList() {
		return m_songs.toArray(new Song[m_songs.size()]);
	}
	
	public synchronized void add(Song song) {
		m_songs.add(song);
	}
	
	public void addFolder(File folder) {
		File[] files = (folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".mp3");
			}
		}));
		for (File f : files) {
			m_songs.add(Song.create(f));
		}
	}
}
