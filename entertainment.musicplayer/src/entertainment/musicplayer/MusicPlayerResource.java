package entertainment.musicplayer;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.amdatu.web.rest.doc.Description;

import entertainment.dashboard.Application;

@Path("musicplayer") @Description("Music player application")
public class MusicPlayerResource {
	volatile Application m_player;

	@GET @Path("status") @Produces("text/json") @Description("Returns the current status of the music player")
	public Map<String, String> status() {
		return m_player.getStatus();
	}
		
	@GET @Path("play") @Produces("text/plain") @Description("Play a song")
	public String play() throws Exception {
		return m_player.execute("play");
	}
	
	@GET @Path("stop") @Produces("text/plain") @Description("Stop playing a song")
	public String stop() throws Exception {
		return m_player.execute("stop");
	}
	
	@GET @Path("appname") @Produces("text/plain") @Description("Returns name of the application")
	public String appname() {
		return m_player.getName();
	}
}
