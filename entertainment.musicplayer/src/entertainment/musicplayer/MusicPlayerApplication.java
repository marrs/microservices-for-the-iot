package entertainment.musicplayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import entertainment.dashboard.Application;

public class MusicPlayerApplication implements Application {
	private static final String KEY_PLAYER = "player";
	private static final String KEY_SONG = "song";
	private Map<String, String> m_status = new HashMap<>();
	private volatile Player m_player;
	private volatile PlayList m_playList;

	public String getName() {
		return "Music Player";
	}

	public Map<String, String> getStatus() {
		m_status.put(KEY_PLAYER, m_player.isPlaying() ? "playing" : "not playing");
		m_status.put(KEY_SONG, m_player.getSongName());
		return m_status;
	}
	
	public String execute(String cmd) throws Exception {
		if (cmd.equals("play")) {
			m_player.playSong();
		}
		else if (cmd.equals("stop")) {
			m_player.stopSong();
		}
		else if (cmd.startsWith("add ")) {
			m_playList.addFolder(new File(cmd.substring(4)));
		}
		return "";
	}
}
