package entertainment.musicplayer;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

import entertainment.dashboard.Application;

public class Activator extends DependencyActivatorBase {
	@SuppressWarnings("serial")
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		dm.add(createComponent()
			.setInterface(Application.class.getName(), new Properties() {{
				put(RemoteConstants.SERVICE_EXPORTED_INTERFACES, Application.class.getName());
				}})
			.setImplementation(MusicPlayerApplication.class)
			.add(createServiceDependency().setService(Player.class).setRequired(true))
			.add(createServiceDependency().setService(PlayList.class).setRequired(true))
		);

		dm.add(createComponent()
			.setInterface(Object.class.getName(), null)
			.setImplementation(MusicPlayerResource.class)
			.add(createServiceDependency().setService(Application.class).setRequired(true))
		);

		dm.add(createComponent()
			.setInterface(Object.class.getName(), new Properties() {{
				put(CommandProcessor.COMMAND_SCOPE, "app");
				put(CommandProcessor.COMMAND_FUNCTION, new String[] { "status", "exec" });
				}})
			.setImplementation(ApplicationShell.class)
			.add(createServiceDependency().setService(Application.class).setRequired(true))
		);

		dm.add(createComponent()
			.setInterface(Player.class.getName(), null)
			.setImplementation(Player.class)
			.add(createServiceDependency().setService(PlayList.class).setRequired(true))
		);

		dm.add(createComponent()
			.setInterface(PlayList.class.getName(), null)
			.setImplementation(PlayList.class)
		);
	}
}
